import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:untitled2/provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => CommonNames()),
        ChangeNotifierProvider(create: (context) => NameButton()),
        ChangeNotifierProvider(create: (context) => CurrencyName()),
        ChangeNotifierProvider(create: (context) => QuotationNumber()),
        ChangeNotifierProvider(create: (context) => Percent()),
        ChangeNotifierProvider(create: (context) => MinusOrPlusCache()),
        ChangeNotifierProvider(create: (context) => MainScreens()),
        ChangeNotifierProvider(create: (context) => Time()),
        ChangeNotifierProvider(create: (context) => Screen()),
        ChangeNotifierProvider(create: (context) => Crypt()),
        ChangeNotifierProvider(create: (context) => Usdt()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const MyHomePage(title: 'Flutter Demo Home Page'),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int selectedIndex = 0;

  void tapScreen(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: selectedIndex,
        onTap: tapScreen,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Главная',
            backgroundColor: Colors.green,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.attach_money),
            label: 'Криптовалюта',
            backgroundColor: Colors.yellow,
          ),
        ],
      ),
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
      ),
      body: Provider.of<MainScreens>(context).mainScreens[selectedIndex],
    );
  }
}
