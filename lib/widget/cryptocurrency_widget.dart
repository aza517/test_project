import 'package:flutter/material.dart';
import 'package:untitled2/provider/provider.dart';
import 'package:provider/provider.dart';
import 'package:untitled2/widget/size.dart';

class CryptocurrencyWidget extends StatelessWidget {
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        padding: const EdgeInsets.all(
          8.0,
        ),
        itemCount: Provider.of<CurrencyName>(context).currencyName.length,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            children: [
              Divider(
                height: getProportionateScreenHeight(1.0),
                color: Colors.grey[600],
              ),
              Container(
                margin: EdgeInsets.all(
                  2.0,
                ),
                height: getProportionateScreenHeight(60.0),
                width: getProportionateScreenWidth(400.0),
                color: Colors.white,
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        Provider.of<CurrencyName>(context).currencyName[index],
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        Provider.of<QuotationNumber>(context)
                            .quotationNumber[index]
                            .toString(),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        left: 15.0,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            Provider.of<Percent>(context).percent[index],
                            style: TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.w600,
                              fontSize: 16.0,
                            ),
                          ),
                          Text(
                            Provider.of<MinusOrPlusCache>(context)
                                .minusOrPlusCache[index],
                            style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.grey[600],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
