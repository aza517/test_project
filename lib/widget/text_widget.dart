import 'package:flutter/material.dart';

class TextWidget extends StatelessWidget {
  final String text;

  const TextWidget({super.key, required this.text});

  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          text,
          style: TextStyle(
            color: Colors.grey[600],
            fontSize: 14.0,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            right: 5.0,
          ),
          child: Column(
            children: [
              Transform.translate(
                offset: Offset(0, 8),
                child: Icon(
                  Icons.arrow_drop_up,
                  color: Colors.grey[600],
                  size: 15,
                ),
              ),
              Transform.translate(
                offset: Offset(0, -3),
                child: Icon(
                  Icons.arrow_drop_down,
                  color: Colors.grey[600],
                  size: 15,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
