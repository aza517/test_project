import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SizeConfig {
  static MediaQueryData? _mediaQueryData;
  static double? screenWidth;
  static double? screenHeight;
  static double? defaultSize;
  static Orientation? orientation;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData!.size.width;
    screenHeight = _mediaQueryData!.size.height;
    orientation = _mediaQueryData!.orientation;
  }
}

// Get the proportionate height as per screen size
double getProportionateScreenHeight(double? inputHeight) {
  double? screenHeight = SizeConfig.screenHeight;
  // print("screenHeight: ${screenHeight}");
  // 812 is the layout height that designer use
  return (inputHeight! / 812.0) * screenHeight!;
}

// Get the proportionate height as per screen size
double getProportionateScreenWidth(double? inputWidth) {
  double? screenWidth = SizeConfig.screenWidth;
  // print("screenWidth: ${screenWidth}");
  // 375 is the layout width that designer use
  return (inputWidth! / 375.0) * screenWidth!;
}

double getDevicePixel(double? pixel) {
  return pixel! * SizeConfig._mediaQueryData!.devicePixelRatio;
}

class SizeConfigAdaptation {
  static double? _screenWidth;
  static double? _screenHeight;
  static double? _blockWidth = 0;
  static double? _blockHeight = 0;

  static double? textMultiplier;
  static double? imageSizeMultiplier;
  static double? heightMultiplier;
  static double? widthMultiplier;
  static bool isPortrait = true;
  static bool isMobilePortrait = false;
  bool enableRotation = false;
  static BoxConstraints? _constraints;
  static MediaQueryData? _mediaQueryData;
  static Orientation? orientation;

  void init(BoxConstraints? constraints, Orientation? orientation,
      {enableRotation}) {
    _constraints = constraints;
    if (orientation == Orientation.portrait) {
      _screenWidth = _constraints!.maxWidth;
      _screenHeight = _constraints!.maxHeight;
      isPortrait = true;
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
    }

    if (_screenWidth! < 450) {
      isMobilePortrait = true;
      if (enableRotation == true) {
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
          DeviceOrientation.portraitDown,
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight,
        ]);
      }
    } else {
      _screenWidth = constraints!.maxHeight;
      _screenHeight = constraints.maxWidth;
      isPortrait = false;
      isMobilePortrait = false;
    }

    _blockWidth = _screenWidth! / 100;
    _blockHeight = _screenHeight! / 100;

    textMultiplier = _blockHeight;
    imageSizeMultiplier = _blockWidth;
    heightMultiplier = _blockHeight;
    widthMultiplier = _blockWidth;

    print("_screenHeight $_screenHeight");
    print("_widthMultiplier $widthMultiplier");
    print("_heightMultiplier $heightMultiplier");
    print("_blockWidth $_blockWidth");
    print("_blockHeight $_blockHeight");
    print("textMultiplier $textMultiplier");
  }

// НЕСТЕРАТЬ НЕ ТРОГАТЬ!!!!!

// void initOrientation(BuildContext context, enableRotations) {
//   _mediaQueryData = MediaQuery.of(context);
//   orientation = _mediaQueryData.orientation;
//
//   if (orientation == Orientation.portrait) {
//     _screenWidth = _constraints.maxWidth;
//     _screenHeight = _constraints.maxHeight;
//     isPortrait = true;
//
//     if (_screenWidth < 450) {
//       isMobilePortrait = true;
//       if (enableRotations == true) {
//         SystemChrome.setPreferredOrientations([
//           DeviceOrientation.portraitUp,
//           DeviceOrientation.portraitDown,
//           DeviceOrientation.landscapeLeft,
//           DeviceOrientation.landscapeRight,
//         ]);
//       }
//     }
//   } else {
//     _screenWidth = _constraints.maxHeight;
//     _screenHeight = _constraints.maxWidth;
//     isPortrait = false;
//     isMobilePortrait = false;
//   }
//
//   _blockWidth = _screenWidth / 100;
//   _blockHeight = _screenHeight / 100;
//
//   textMultiplier = _blockHeight;
//   imageSizeMultiplier = _blockWidth;
//   heightMultiplier = _blockHeight;
//   widthMultiplier = _blockWidth;
//
//   print("initOrientation blockHeight $_blockHeight");
// }
}
//
