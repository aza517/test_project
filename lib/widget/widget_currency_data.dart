import 'package:flutter/material.dart';

class CurrencyData extends StatelessWidget {
  final String text;
  final String textNumber;
  const CurrencyData({
    super.key,
    required this.text,
    required this.textNumber,
  });

  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(
            left: 8.0,
            top: 5.0,
          ),
          child: Text(
            text,
            // Provider.of<Crypt>(context).crypt[0],
            style: TextStyle(
              color: Colors.grey[600],
              fontSize: 18.0,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
            left: 8.0,
            top: 5.0,
          ),
          child: Text(
            textNumber,
            style: TextStyle(
              color: Colors.black,
              fontSize: 18.0,
            ),
          ),
        ),
      ],
    );
  }
}
