import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:untitled2/widget/size.dart';

class DividerBox extends StatelessWidget {
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: 10.0,
      ),
      height: getProportionateScreenHeight(10.0),
      width: getProportionateScreenWidth(400.0),
      color: Colors.grey[300],
    );
  }
}
