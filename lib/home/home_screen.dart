import 'package:flutter/material.dart';
import 'package:untitled2/provider/provider.dart';

import 'package:untitled2/widget/cryptocurrency_widget.dart';

import 'package:untitled2/widget/size.dart';
import 'package:provider/provider.dart';

import 'package:untitled2/widget/text_widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: body(context),
    );
  }

  Widget body(context) {
    return Column(
      children: [
        Container(
          height: getProportionateScreenHeight(40.0),
          width: getProportionateScreenWidth(400.0),
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Transform.translate(
                offset: const Offset(10, 0),
                child: Text(
                  Provider.of<CommonNames>(context).commonNames[0],
                  style: const TextStyle(
                      color: Colors.black,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Transform.translate(
                offset: const Offset(-10, 0),
                child: InkWell(
                  onTap: () {},
                  child: const Icon(
                    Icons.search,
                    color: Colors.black,
                    size: 30.0,
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
            left: 5.0,
            top: 35.0,
          ),
          child: Row(
            children: [
              const Icon(
                Icons.search,
                color: Color(0xff507e92),
                size: 15,
              ),
              Expanded(
                flex: 4,
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 8.0,
                  ),
                  child: Text(
                    Provider.of<NameButton>(context).nameButton[0],
                    style: const TextStyle(
                      color: Color(0xff507e92),
                      fontSize: 14.0,
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: TextWidget(
                  text: Provider.of<NameButton>(context).nameButton[1],
                ),
              ),
              TextWidget(
                text: Provider.of<NameButton>(context).nameButton[2],
              ),
            ],
          ),
        ),
        CryptocurrencyWidget()
      ],
    );
  }
}
