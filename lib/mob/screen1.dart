import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:untitled2/model/api_constats.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:untitled2/provider/provider.dart';
import 'package:untitled2/widget/divider_custom.dart';
import 'package:untitled2/widget/size.dart';
import 'package:untitled2/widget/widget_currency_data.dart';

class Screen1 extends StatefulWidget {
  @override
  State<Screen1> createState() => _Screen1State();
}

class _Screen1State extends State<Screen1> {
  List quotes = [];

  Future<void> _getQuotes() async {
    final url = Uri.parse(ApiConstats.baseUrl + ApiConstats.usersEndpoint);
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      setState(() {
        quotes = data.values.toList().first;
      });
    } else {
      throw Exception(
        'Failed to load quotes.',
      );
    }
  }

  List<CryptoData> data = [
    CryptoData(4, 50.56, 5.5, 4, 4),
    CryptoData(5, 49.42, 25.25, 5, 4),
    CryptoData(6, 53.21, 69.63, 62, 4),
    CryptoData(7, 64.78, 523.6987, 88, 4),
    CryptoData(8, 59.100, 9898.989, 5454, 4),
  ];

  @override
  Widget build(BuildContext context) {
    _getQuotes();
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: getProportionateScreenHeight(200.0),
            child: SfCartesianChart(
              isTransposed: true,
              primaryXAxis: CategoryAxis(),
              series: <LineSeries<CryptoData, String>>[
                LineSeries<CryptoData, String>(
                  dataSource: data,
                  name: 'Sales',
                  xValueMapper: (CryptoData sales, _) => sales.time.toString(),
                  yValueMapper: (CryptoData sales, _) => sales.close,
                  dataLabelSettings: const DataLabelSettings(
                    isVisible: true,
                  ),
                ),
              ],
            ),
          ),
          DividerBox(),
          Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: CurrencyData(
                      text: Provider.of<Crypt>(context).crypt[0],
                      textNumber: Provider.of<Usdt>(context).usdt[0],
                    ),
                  ),
                  Expanded(
                    child: CurrencyData(
                      text: Provider.of<Crypt>(context).crypt[1],
                      textNumber: Provider.of<Usdt>(context).usdt[1],
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: CurrencyData(
                      text: Provider.of<Crypt>(context).crypt[0],
                      textNumber: Provider.of<Usdt>(context).usdt[0],
                    ),
                  ),
                  Expanded(
                    child: CurrencyData(
                      text: Provider.of<Crypt>(context).crypt[1],
                      textNumber: Provider.of<Usdt>(context).usdt[1],
                    ),
                  ),
                ],
              ),
              DividerBox()
            ],
          ),
        ],
      ),
    );
  }
}

class CryptoData {
  CryptoData(
    this.open,
    this.close,
    this.high,
    this.low,
    this.time,
  );

  final double open;
  final double close;
  final double high;
  final double low;
  final int time;
}
