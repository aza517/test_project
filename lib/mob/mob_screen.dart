import 'package:flutter/material.dart';

import 'package:untitled2/provider/provider.dart';
import 'package:untitled2/widget/divider_custom.dart';
import 'package:untitled2/widget/size.dart';
import 'package:provider/provider.dart';

class MobScreen extends StatefulWidget {
  @override
  State<MobScreen> createState() => _MobScreenState();
}

class _MobScreenState extends State<MobScreen> {
  int current = 0;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          Provider.of<CommonNames>(context).commonNames[1],
          style: TextStyle(
            color: Colors.black,
            fontSize: 25.0,
            fontWeight: FontWeight.w600,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
      ),
      body: body(),
    );
  }

  Widget body() {
    return Column(
      children: [
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 14.0,
                right: 8.0,
              ),
              child: Text(
                Provider.of<CommonNames>(context).commonNames[2],
                style: TextStyle(
                  fontSize: 18.0,
                  color: Color(0xff507e92),
                ),
              ),
            ),
            Text(
              Provider.of<QuotationNumber>(context)
                  .quotationNumber[0]
                  .toString(),
              style: TextStyle(
                color: Colors.black,
                fontSize: 30.0,
                fontWeight: FontWeight.w600,
              ),
            ),
          ],
        ),
        DividerBox(),
        Container(
          padding: EdgeInsets.only(
            top: 6.0,
            left: 15.0,
            right: 15.0,
          ),
          height: getProportionateScreenHeight(50.0),
          width: getProportionateScreenWidth(400.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: List.generate(
              Provider.of<Time>(context).time.length,
              (index) => InkWell(
                onTap: () {
                  setState(() {
                    current = index;
                  });
                },
                child: Container(
                  padding: const EdgeInsets.all(2.0),
                  width: getProportionateScreenWidth(60.0),
                  height: getProportionateScreenHeight(25.0),
                  decoration: BoxDecoration(
                    color: current == index ? Colors.red : Colors.white54,
                    borderRadius: current == index
                        ? BorderRadius.circular(10)
                        : BorderRadius.circular(10),
                    border: current == index
                        ? Border.all(
                            color: Colors.red!,
                            width: 2,
                          )
                        : null,
                  ),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      Provider.of<Time>(context).time[index],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        Column(
          children: [DividerBox()],
        ),
        Container(
          child: Expanded(
            child: Provider.of<Screen>(context).screen[current],
          ),
        ),
      ],
    );
  }
}
// Container(
// padding: const EdgeInsets.all(3.0),
// width: getProportionateScreenWidth(55.0),
// height: getProportionateScreenHeight(55.0),
// decoration: BoxDecoration(
// color: initialIndex == index
// ? Colors.white70
//     : Colors.white54,
// borderRadius: initialIndex == index
// ? BorderRadius.circular(15)
// : BorderRadius.circular(10),
// border: initialIndex == index
// ? Border.all(
// color: Colors.blueAccent[700]!,
// width: 2,
// )
// : null,
// ),
// child:
