import 'package:flutter/material.dart';
import 'package:untitled2/home/home_screen.dart';
import 'package:untitled2/mob/mob_screen.dart';
import 'package:untitled2/mob/screen1.dart';

class CommonNames extends ChangeNotifier {
  List<String> commonNames = [
    'Криптовалюта',
    'BTC/USDT',
    'цена:',
  ];
}

class NameButton extends ChangeNotifier {
  List<String> nameButton = [
    'Тикер/Название',
    'Цена',
    'Изм. %/;',
  ];
}

class CurrencyName extends ChangeNotifier {
  List<String> currencyName = [
    'BTC/USDT',
    'ETH/USDT',
    'XRP/USDT',
    'XRP/ETH',
    'ETH/BTC',
    'ADA/USDT',
    'ETH/USDT',
    'BTC/USDT',
    'ETH/BTC',
    'BTC/USDT',
  ];
}

class QuotationNumber extends ChangeNotifier {
  List<double> quotationNumber = [
    32340.52940,
    56.596,
    1380.43,
    35.43,
    35.43,
    1380.43,
    1380.43,
    1380.43,
    1380.43,
    1380.43,
  ];
}

class Percent extends ChangeNotifier {
  List<String> percent = [
    '+4.08%',
    '+94.08%',
    '+4.08%',
    '+4.08%',
    '+4.08%',
    '+4.08%',
    '+4.08%',
    '+4.08%',
    '+4.08%',
    '+4.08%',
  ];
}

class MinusOrPlusCache extends ChangeNotifier {
  List<String> minusOrPlusCache = [
    '+0.37',
    '-0.37',
    '-1234.23',
    '-0.37',
    '-0.37',
    '+0.37',
    '-0.37',
    '-0.37',
    '-0.37',
    '-0.37',
  ];
}

class Screen extends ChangeNotifier {
  List<Widget> screen = [
    Screen1(),
  ];
}

class MainScreens extends ChangeNotifier {
  List<Widget> mainScreens = [HomeScreen(), MobScreen()];
}

class Time extends ChangeNotifier {
  List<String> time = [
    '1Д',
    '5Д',
    '1Н',
    '1МЕС',
    '3МЕС',
  ];
}

class Crypt extends ChangeNotifier {
  List<String> crypt = [
    'HIGH:',
    'LOW:',
    'OPEN:',
    'CLOSE:',
  ];
}

class Usdt extends ChangeNotifier {
  List<String> usdt = [
    '32.323 USDT',
    '31.098 USDT',
  ];
}
